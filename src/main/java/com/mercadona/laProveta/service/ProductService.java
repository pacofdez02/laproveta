package com.mercadona.laProveta.service;

import com.mercadona.laProveta.dto.AssortmentDto;
import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.entity.Supplier;
import com.mercadona.laProveta.exception.NoProductAvailableException;
import com.mercadona.laProveta.mapper.AssortmentMapper;
import com.mercadona.laProveta.mapper.ProductMapper;
import com.mercadona.laProveta.mapper.SupplierMapper;
import com.mercadona.laProveta.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.hibernate5.HibernateOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;


    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }

    public Page<Product> getAllProducts(org.springframework.data.domain.Pageable pageable) {
        return productRepository.findAll(pageable);
    }


    public ProductDto getOneProductById(Long id){
        Product p = productRepository.findById(id).get();
        //ist<SupplierDto> listSupplierDto = SupplierMapper.INSTANCE.aDtoList(p.getSuppliers());
        //List<Assortment> listAssortment = productRepository.findAssortmentsByProductId(p.getId());
        //p.setAssortments(listAssortment);
        ProductDto pDto = ProductMapper.INSTANCE.aDto(p);
        //pDto.setSuppliersDto(listSupplierDto);
        //pDto.setAssortmentsDto(listAssortmentDto);
        return pDto;
    }

    @ExceptionHandler(NoProductAvailableException.class)
    public ResponseEntity<String> handleMiExcepcion(NoProductAvailableException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    public Product saveProduct(Product product){
        return productRepository.save(product);
    }

    public Product updateProduct(Product product, Long id){
        Product newProduct = productRepository.findById(id).orElseThrow();
        newProduct.setNombre(product.getNombre());
        productRepository.save(newProduct);
        return newProduct;
    }

    public void deleteProduct(Long id){
        Product newProduct = productRepository.findById(id).orElseThrow();
        productRepository.delete(newProduct);
    }


}
