package com.mercadona.laProveta.service;

import com.mercadona.laProveta.dto.PaleDto;
import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.entity.Pale;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.exception.MyException;
import com.mercadona.laProveta.repository.PaleRepository;
import com.mercadona.laProveta.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class EanService {

    private final ProductService productService;
    private final PaleRepository paleRepository;


    private String ean128="";
    private PaleDto paleDto;
    private ProductDto productDto;


    public PaleDto getPaleByEanValidation(String ean128){
        this.ean128 = ean128;
        paleDto = new PaleDto();
        productDto = new ProductDto();
        validarEan();
        paleDto.setIdArticulo(productDto.getId());
        paleDto.setNombreArticulo(productDto.getNombre());
        if(paleDto.getIdArticulo() == null  || paleDto.getNombreArticulo() == null || paleDto.getPesoNeto() == null || paleDto.getNumeroLote() == null || paleDto.getFechaFabricacion() == null)
            throw new MyException("El pale no ha sido bien procesado");

        return paleDto;
    }

    public Pale savePale(String pale){
        ean128 = pale;
        Pale paleP = new Pale();
        //todo refactor duplicado codigo
        validarEan();
        paleDto.setIdArticulo(productDto.getId());
        paleDto.setNombreArticulo(productDto.getNombre());
        if(paleDto.getIdArticulo() == null  || paleDto.getNombreArticulo() == null || paleDto.getPesoNeto() == null || paleDto.getNumeroLote() == null || paleDto.getFechaFabricacion() == null)
            throw new MyException("El pale no ha sido bien procesado");
        return paleRepository.save(paleP);
    }


    @ExceptionHandler(MyException.class)
    public ResponseEntity<String> handleMiExcepcion(MyException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }



    public void validarEan(){
        for (int i = 0; i < ean128.length()-1;) {
            if(ean128.charAt(i) == '0' && ean128.charAt(i+1) == '0'){
                paleDto.setIdPale(Long.parseLong(ean128.substring(2,20)));
                this.ean128 = ean128.substring(20);

           }

            else if(ean128.charAt(i) == '0' && ean128.charAt(i+1) == '1'){
                productDto.setId(Long.valueOf(ean128.substring(2,16)));
                productDto.setNombre(productService.getOneProductById(productDto.getId()).getNombre());
                this.ean128 = ean128.substring(16);

            }

            else if(ean128.charAt(i) == '1' && ean128.charAt(i+1) == '0'){
                if(!ean128.contains("*")) {
                    paleDto.setNumeroLote(ean128.substring(2));
                    break;
                }
                int antes = ean128.split("\\*")[0].length();
                paleDto.setNumeroLote(ean128.substring(2, antes));
                this.ean128 = String.valueOf(ean128.split("\\*")[1]);

            }
            else if(ean128.charAt(i) == '1' && ean128.charAt(i+1) == '1'){
                String substring = ean128.substring(8);
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMuu");
                paleDto.setFechaFabricacion(LocalDate.parse(ean128.substring(2, 8),formatter));
                this.ean128 = substring;

            }

            else if(ean128.charAt(i) == '3' && ean128.charAt(i+1) == '1' && ean128.charAt(i+2) == '0'){
                String substring = ean128.substring(10);
                int comaPos = ean128.indexOf(i+3);
                long valor = Long.parseLong(ean128.substring(3, 10));
                //valor = +Math.pow(10,comaPos);
                paleDto.setPesoNeto(valor);

                this.ean128 = ean128.substring(10);

            }

        }


    }




}
