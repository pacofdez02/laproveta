package com.mercadona.laProveta.service;

import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Supplier;
import com.mercadona.laProveta.exception.NoProductAvailableException;
import com.mercadona.laProveta.mapper.SupplierMapper;
import com.mercadona.laProveta.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SupplierService {

    @Autowired
    private final SupplierRepository SupplierRepository;


    public List<Supplier> getAllSuppliers(){
        return SupplierRepository.findAll();
    }

    public SupplierDto getOneSupplierById(Long id){
        Supplier a = SupplierRepository.findById(id).get();
        SupplierDto supplierDto = SupplierMapper.INSTANCE.aDto(a);
        return supplierDto;
    }

    //todo implementar
    @ExceptionHandler(NoProductAvailableException.class)
    public ResponseEntity<String> handleMiExcepcion(NoProductAvailableException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    public Supplier saveSupplier(SupplierDto supplierDto){
        Supplier supplier = SupplierMapper.INSTANCE.aDominio(supplierDto);
        return SupplierRepository.save(supplier);
    }

    public Supplier updateSupplier(Supplier supplier, Long id){
        Supplier newSupplier = SupplierRepository.findById(id).orElseThrow();
        SupplierRepository.save(newSupplier);
        return newSupplier;
    }

    public void deleteSupplier(Long id){
        Supplier newSupplier = SupplierRepository.findById(id).orElseThrow();
        SupplierRepository.delete(newSupplier);
    }


}
