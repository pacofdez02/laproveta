package com.mercadona.laProveta.service;

import com.mercadona.laProveta.dto.AssortmentDto;
import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.exception.NoProductAvailableException;
import com.mercadona.laProveta.mapper.AssortmentMapper;
import com.mercadona.laProveta.mapper.ProductMapper;
import com.mercadona.laProveta.mapper.SupplierMapper;
import com.mercadona.laProveta.repository.AssortmentRepository;
import com.mercadona.laProveta.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AssortmentService {

    @Autowired
    private final AssortmentRepository assortmentRepository;


    public List<Assortment> getAllAssortments(){
        return assortmentRepository.findAll();
    }

    public AssortmentDto getOneAssortmentById(Integer id){
        Assortment a = assortmentRepository.findById(id).get();
        AssortmentDto assortDto = AssortmentMapper.INSTANCE.aDto(a);
        return assortDto;
    }

    //todo implementar
    @ExceptionHandler(NoProductAvailableException.class)
    public ResponseEntity<String> handleMiExcepcion(NoProductAvailableException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    public Assortment saveAssortment(AssortmentDto assortmentDto){
        Assortment assortment = AssortmentMapper.INSTANCE.aDominio(assortmentDto);
        return assortmentRepository.save(assortment);
    }

    public Assortment updateProduct(Assortment assortment, Integer id){
        Assortment newAssortment = assortmentRepository.findById(id).orElseThrow();
        assortmentRepository.save(newAssortment);
        return newAssortment;
    }

    public void deleteAssortment(Integer id){
        Assortment newAssortment = assortmentRepository.findById(id).orElseThrow();
        assortmentRepository.delete(newAssortment);
    }


}
