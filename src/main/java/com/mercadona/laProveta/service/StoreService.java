package com.mercadona.laProveta.service;

import com.mercadona.laProveta.dto.StoreDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Store;
import com.mercadona.laProveta.entity.Supplier;
import com.mercadona.laProveta.exception.NoProductAvailableException;
import com.mercadona.laProveta.mapper.StoreMapper;
import com.mercadona.laProveta.mapper.SupplierMapper;
import com.mercadona.laProveta.repository.StoreRepository;
import com.mercadona.laProveta.repository.SupplierRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreService {

    @Autowired
    private final StoreRepository storeRepository;


    public List<Store> getAllStores(){
        return storeRepository.findAll();
    }

    public StoreDto getOneStoreById(Long id){
        Store a = storeRepository.findById(id).get();
        StoreDto storeDto = StoreMapper.INSTANCE.aDto(a);
        return storeDto;
    }

    //todo implementar
    @ExceptionHandler(NoProductAvailableException.class)
    public ResponseEntity<String> handleMiExcepcion(NoProductAvailableException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }


    public Store saveStore(StoreDto storeDto){
        Store store = StoreMapper.INSTANCE.aDominio(storeDto);
        return storeRepository.save(store);
    }

    public Store updateStore(Store store, Long id){
        Store newStore = storeRepository.findById(id).orElseThrow();
        storeRepository.save(newStore);
        return newStore;
    }

    public void deleteStore(Long id){
        Store newStore = storeRepository.findById(id).orElseThrow();
        storeRepository.delete(newStore);
    }


}
