package com.mercadona.laProveta.mapper;

import com.mercadona.laProveta.dto.*;
import com.mercadona.laProveta.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = SupplierMapper.class)
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper( ProductMapper.class );

    @Mapping(source = "suppliersDto", target = "suppliers")
    @Mapping(source = "assortmentsDto", target = "assortments")
    Product aDominio (ProductDto productDto);

    @Mapping(source = "suppliers", target = "suppliersDto")
    @Mapping(source = "assortments", target = "assortmentsDto")
    ProductDto aDto (Product product);


}