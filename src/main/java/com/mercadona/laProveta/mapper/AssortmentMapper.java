package com.mercadona.laProveta.mapper;

import com.mercadona.laProveta.dto.AssortmentDto;
import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.entity.Supplier;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = ProductMapper.class)
public interface AssortmentMapper {

    AssortmentMapper INSTANCE = Mappers.getMapper( AssortmentMapper.class );

    Assortment aDominio (AssortmentDto assortmentDto);

    @Mapping(source = "product", target = "productDto")
    AssortmentDto aDto (Assortment assortment);
    @Mapping(source = "product", target = "productDto")
    List<AssortmentDto> aDtoList (List<Assortment> assortments);

}
