package com.mercadona.laProveta.mapper;

import com.mercadona.laProveta.dto.AddressDto;
import com.mercadona.laProveta.dto.AssortmentDto;
import com.mercadona.laProveta.entity.Address;
import com.mercadona.laProveta.entity.Assortment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AddressMapper {

    AddressMapper INSTANCE = Mappers.getMapper( AddressMapper.class );

    Address aDominio (AddressDto addressDto);

    AddressDto aDto (Address address);


}
