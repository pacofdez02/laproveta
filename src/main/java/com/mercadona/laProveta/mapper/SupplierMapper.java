package com.mercadona.laProveta.mapper;

import com.mercadona.laProveta.dto.*;
import com.mercadona.laProveta.entity.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = AddressMapper.class)
public interface SupplierMapper {
    SupplierMapper INSTANCE = Mappers.getMapper( SupplierMapper.class );

    @Mapping(source = "productsDto",target = "products")
    @Mapping(source = "addressDto",target = "address")
    Supplier aDominio (SupplierDto supplier);
    @Mapping(source = "products",target = "productsDto")
    @Mapping(source = "address",target = "addressDto")
    SupplierDto aDto (Supplier supplier);

    @Mapping(source = "products",target = "productsDto")
    @Mapping(source = "address",target = "addressDto")
    List<SupplierDto> aDtoList (List<Supplier> suppliers);




}