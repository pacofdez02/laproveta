package com.mercadona.laProveta.mapper;

import com.mercadona.laProveta.dto.StoreDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Store;
import com.mercadona.laProveta.entity.Supplier;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface StoreMapper {
    StoreMapper INSTANCE = Mappers.getMapper( StoreMapper.class );

    Store aDominio (StoreDto store);

    StoreDto aDto (Store store);






}