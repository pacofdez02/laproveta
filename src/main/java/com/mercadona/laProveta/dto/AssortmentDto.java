package com.mercadona.laProveta.dto;

import com.mercadona.laProveta.entity.Store;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AssortmentDto {
    private int id;
    private int units;
    private StoreDto store;
}
