package com.mercadona.laProveta.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;
import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaleDto {

    private Long idPale;
    private Long idArticulo;
    private String nombreArticulo;
    private String numeroLote;
    private LocalDate fechaFabricacion;
    private Long pesoNeto;

}
