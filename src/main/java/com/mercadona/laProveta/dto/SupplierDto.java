package com.mercadona.laProveta.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SupplierDto {
    private Long id;
    private String nombre;
    private String iban;
    private AddressDto addressDto;
    @JsonIgnore
    private List<ProductDto> productsDto;

}
