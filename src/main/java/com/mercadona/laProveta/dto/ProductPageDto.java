package com.mercadona.laProveta.dto;

import java.util.List;

public class ProductPageDto {
    private List<ProductDto> data;
    private PaginationDto pagination;
}
