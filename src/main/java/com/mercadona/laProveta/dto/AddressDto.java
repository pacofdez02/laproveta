package com.mercadona.laProveta.dto;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mercadona.laProveta.entity.Supplier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
    private Long id;
    private String country;
    private String city;
    private String state;
    private String postalCode;
    private String street;
    @JsonIgnore
    private Supplier supplier;

}
