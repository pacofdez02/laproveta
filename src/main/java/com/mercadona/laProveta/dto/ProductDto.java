package com.mercadona.laProveta.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.mercadona.laProveta.entity.Supplier;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.ElementCollection;
import javax.persistence.FetchType;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private Long id;
    private String nombre;
    private String description;
    private String image_url;
    private double price;
    private Set<String> tags;
    private List<SupplierDto> suppliersDto;
    private List<AssortmentDto> assortmentsDto;

}
