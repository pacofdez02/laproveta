package com.mercadona.laProveta.dto;

public class PaginationDto {
    private int currentPage;
    private int totalPages;
    private int numItems;
    private int totalItems;
}
