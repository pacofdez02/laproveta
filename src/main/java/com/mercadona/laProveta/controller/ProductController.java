package com.mercadona.laProveta.controller;


import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.service.ProductService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/products")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;


/*
    @GetMapping
    public List<Product> getAllProducts()  {
        return productService.getAllProducts();
    }*/

    @GetMapping
    public Page<Product> getAllProducts(@PageableDefault(page = 0, size = 5) org.springframework.data.domain.Pageable pageable) {
        return productService.getAllProducts((Pageable) pageable);
    }

    @Transactional
    @GetMapping("/{id}")
    public ProductDto getOneProductById(@PathVariable Long id){
        return productService.getOneProductById(id);
    }
    @Transactional
    @PostMapping
    public Product saveProduct(@RequestBody Product product){
        return productService.saveProduct(product);
    }

    @PutMapping("/{id}")
    public Product updateProduct(@RequestBody Product product, @PathVariable Long id){
        return productService.updateProduct(product, id);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id){
        productService.deleteProduct(id);
    }


}
