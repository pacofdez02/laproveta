package com.mercadona.laProveta.controller;


import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Supplier;
import com.mercadona.laProveta.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/suppliers")
@RequiredArgsConstructor
public class SupplierController {

    private final SupplierService SupplierService;

    @Transactional
    @GetMapping
    public List<Supplier> getAllSuppliers()  {
        return SupplierService.getAllSuppliers();
    }


    @Transactional
    @GetMapping("/{id}")
    public SupplierDto getOneSupplierById(@PathVariable Long id){
        return SupplierService.getOneSupplierById(id);
    }
    @PostMapping
    public Supplier saveSupplier(@RequestBody SupplierDto supplierDto){
        return SupplierService.saveSupplier(supplierDto);
    }

    @PutMapping("/{id}")
    public Supplier updateSupplier(@RequestBody Supplier supplier, @PathVariable Long id){
        return updateSupplier(supplier,id);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Long id){
        SupplierService.deleteSupplier(id);
    }


}
