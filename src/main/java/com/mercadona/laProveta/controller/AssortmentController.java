package com.mercadona.laProveta.controller;


import com.mercadona.laProveta.dto.AssortmentDto;
import com.mercadona.laProveta.dto.ProductDto;
import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.service.AssortmentService;
import com.mercadona.laProveta.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/assortments")
@RequiredArgsConstructor
public class AssortmentController {

    private final AssortmentService assortmentService;

    @Transactional
    @GetMapping
    public List<Assortment> getAllAssortments()  {
        return assortmentService.getAllAssortments();
    }


    @Transactional
    @GetMapping("/{id}")
    public AssortmentDto getOneAssortmentById(@PathVariable Integer id){
        return assortmentService.getOneAssortmentById(id);
    }
    @Transactional
    @PostMapping
    public Assortment saveAssortment(@RequestBody AssortmentDto assortmentDto){
        return assortmentService.saveAssortment(assortmentDto);
    }

    @PutMapping("/{id}")
    public Assortment updateAssortment(@RequestBody Assortment assortment, @PathVariable Integer id){
        return updateAssortment(assortment,id);
    }

    @DeleteMapping("/{id}")
    public void deleteProduct(@PathVariable Integer id){
        assortmentService.deleteAssortment(id);
    }


}
