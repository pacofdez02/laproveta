package com.mercadona.laProveta.controller;


import com.mercadona.laProveta.dto.StoreDto;
import com.mercadona.laProveta.dto.SupplierDto;
import com.mercadona.laProveta.entity.Store;
import com.mercadona.laProveta.entity.Supplier;
import com.mercadona.laProveta.service.StoreService;
import com.mercadona.laProveta.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.List;

@RestController
@RequestMapping("/store")
@RequiredArgsConstructor
public class StoreController {

    private final StoreService storeService;

    @Transactional
    @GetMapping
    public List<Store> getAllStores()  {
        return storeService.getAllStores();
    }


    @Transactional
    @GetMapping("/{id}")
    public StoreDto getOneStoreById(@PathVariable Long id){
        return storeService.getOneStoreById(id);
    }

    @Transactional
    @PostMapping
    public Store saveStore(@RequestBody StoreDto storeDto){
        return storeService.saveStore(storeDto);
    }

    @PutMapping("/{id}")
    public Store updateStore(@RequestBody Store store, @PathVariable Long id){
        return updateStore(store,id);
    }

    @DeleteMapping("/{id}")
    public void deleteStore(@PathVariable Long id){
        storeService.deleteStore(id);
    }


}
