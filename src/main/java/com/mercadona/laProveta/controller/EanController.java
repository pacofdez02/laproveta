package com.mercadona.laProveta.controller;

import com.mercadona.laProveta.dto.PaleDto;
import com.mercadona.laProveta.entity.Pale;
import com.mercadona.laProveta.service.EanService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;

@RestController
@RequestMapping("/ean128")
@RequiredArgsConstructor
public class EanController {

    @Autowired
    private final EanService eanService;


    @Transactional
    @GetMapping
    public PaleDto getPale(@RequestParam String ean128){
            return eanService.getPaleByEanValidation(ean128);
    }

    @Transactional
    @PostMapping
    public Pale savePale(@RequestParam String ean128){
        return eanService.savePale(ean128);
    }



}
