package com.mercadona.laProveta.repository;

import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long>{
    Page<Product> findAll(Pageable pageable);

    @Query("SELECT a FROM Assortment a WHERE a.product.id = :productId")
    List<Assortment> findAssortmentsByProductId(@Param("productId") Long productId);
}
