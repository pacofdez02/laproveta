package com.mercadona.laProveta.repository;

import com.mercadona.laProveta.entity.Store;
import com.mercadona.laProveta.entity.Supplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreRepository extends JpaRepository<Store,Long>{
}
