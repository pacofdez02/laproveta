package com.mercadona.laProveta.repository;

import com.mercadona.laProveta.entity.Assortment;
import com.mercadona.laProveta.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssortmentRepository extends JpaRepository<Assortment,Integer>{
}
