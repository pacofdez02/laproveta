package com.mercadona.laProveta.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "Store")
public class Store {
    @Id
    private Long code;
    private String name;

    @JsonIgnore
    @OneToMany(mappedBy = "store", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private List<Assortment> assortments;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<Pale> pales;

}
