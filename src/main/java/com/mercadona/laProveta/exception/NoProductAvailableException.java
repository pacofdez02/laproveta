package com.mercadona.laProveta.exception;

public class NoProductAvailableException extends RuntimeException{
    public NoProductAvailableException(String mensaje) {
        super(mensaje);
    }
}
