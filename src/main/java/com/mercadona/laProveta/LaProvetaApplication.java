package com.mercadona.laProveta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LaProvetaApplication {

	public static void main(String[] args) {
		SpringApplication.run(LaProvetaApplication.class, args);
	}

}
