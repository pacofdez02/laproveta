package com.mercadona.laProveta;

import com.mercadona.laProveta.entity.Product;
import com.mercadona.laProveta.service.ProductService;
import com.mercadona.laProveta.repository.ProductRepository;
import com.mercadona.laProveta.service.EanService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@ContextConfiguration(classes = LaProvetaApplication.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class LaProvetaApplicationTests {


	private final ProductRepository productRepository;

	private final EanService service;

	private final ProductService productService;

	@Autowired
	LaProvetaApplicationTests(ProductRepository productRepository, EanService service, ProductService productService) {
		this.productRepository = productRepository;
		this.service = service;
		this.productService = productService;
	}


	@Test
	@Order(1)
	@Rollback(value = false)
	public void saveProductTest(){

		Product product = Product.builder()
				.id(1L)
				.nombre("Manzana")
				.build();

		productRepository.save(product);

		Assertions.assertThat(product.getId()).isGreaterThan(0);
	}
/*
	@Test
	@Order(2)
	public void getProductTest(){
		Product product = productRepository.findById(1L).get();
		Assertions.assertThat(product.getId()).isEqualTo("1");

	}
*/


}
